﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace PrimeNumbersWithinRange
{
    public class PrimeNumbersWithinRange
    {
        public List<int> PrimeNumbers(int input)
        {
            var test = new List<int>();

            for (var i = 0; i <= input; i++)
            {
                var counter = 0;
                for (var j = 2; j <= i / 2; j++)
                {
                    if (i % j == 0)
                    {
                        counter++;
                        break;
                    }
                }

                if (counter != 0 || i is 0 or 1) continue;
                test.Add(i);
            }

            return test;
        }
        public string Reverse(string s)
        {
            var result = string.Empty;
            var cArr = s.ToCharArray();
            var end = cArr.Length - 1;

            for (var i = end; i >= 0; i--)
            {
                result += cArr[i];
            }
            return result;
        }
    }

    public class PrimeNumbers2
    {
        public List<int> GetPrimeNumbersBetween(int input)
        {
            var primeNumbers = new List<int>();
            for (var i = 0; i <= input; i++)
            {
                var counter = 0;
                for (var j = 2; j <= i / 2; j++)
                {
                    if (i % j == 0)
                    {
                        counter++;
                        break;
                    }
                }

                if (counter != 0 || i is 0 or 1) continue;
                primeNumbers.Add(i);
                
            }
            return primeNumbers;
        }
    }

    [TestFixture]
    public class TestPrimeNumbersWithinRange
    {
        [Test]
        public void Given10_ShouldReturnPrimeNumbers()
        {
            //Arrange
            var sut = new PrimeNumbersWithinRange();
            var expected = new List<int>
            {
                2, 3, 5, 7
            };
            //Act
            var primeNumbers = sut.PrimeNumbers(10);
            //Assert
            Assert.AreEqual(expected, primeNumbers);

        }


        [Test]
        public void Given10_PrimeNumbers2_ShouldReturnPrimeNumbers()
        {
            //Arrange
            var sut = new PrimeNumbers2();
            var expected = new List<int>
            {
                2, 3, 5, 7
            };
            //Act
            var primeNumbers = sut.GetPrimeNumbersBetween(10);
            //Assert
            Assert.AreEqual(expected, primeNumbers);

        }
    }

    [TestFixture]
    public class ReverseString
    {
        [Test]
        public void GivenString_ShouldReturnReverseString()
        {
            //Arrange
            var sut = new PrimeNumbersWithinRange();
            var expected = "kluh gib taerg eht ma i";
            //Act
            var reverse = sut.Reverse("i am the great big hulk");
            //Assert
            Assert.AreEqual(expected, reverse);

        }
    }
}
